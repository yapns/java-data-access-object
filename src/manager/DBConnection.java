/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.logicalcobwebs.proxool.configuration.PropertyConfigurator;    //PropertyConfigurator.java

/**
 *
 * @author naisiong.yap
 */
public class DBConnection {
    private static Logger logger = Logger.getLogger(DBConnection.class.getName());
    private static ResourceBundle resource = ResourceBundle.getBundle("db");
    private static boolean isDBPoolInit = false;
    private static boolean isAortaDBPoolInit = false;

    public  DBConnection(){
    }

    public static void main(String[] args) throws Exception {
        System.out.println("test - start");
        Connection conn = getConnection();
        if (conn != null) {
            System.out.println("connection is not null...got connection");
        } else {
            System.out.println("no connection");
        }
    }
    public static Connection getConnection() throws Exception {
        Connection conn = null;

        if (!isDBPoolInit){
            PropertyConfigurator.configure(init());   //PropertyConfigurator.java
        }
        conn = DriverManager.getConnection("proxool." + resource.getString("jdbc-1.proxool.alias"));
        conn.setAutoCommit(true);
        return conn;
    }

    private static synchronized Properties init() throws Exception {
        try {
            logger.info("Initializing connection pool");
            Properties dbPoolProperties = new Properties();
            dbPoolProperties.setProperty("jdbc-1.proxool.alias", resource.getString("jdbc-1.proxool.alias"));
            dbPoolProperties.setProperty("jdbc-1.proxool.driver-url", resource.getString("jdbc-1.proxool.driver-url"));
            dbPoolProperties.setProperty("jdbc-1.proxool.driver-class", resource.getString("jdbc-1.proxool.driver-class"));
            dbPoolProperties.setProperty("jdbc-1.user", resource.getString("jdbc-1.user"));
            dbPoolProperties.setProperty("jdbc-1.password", resource.getString("jdbc-1.password"));
            dbPoolProperties.setProperty("jdbc-1.proxool.house-keeping-sleep-time", resource.getString("jdbc-1.proxool.house-keeping-sleep-time"));
            dbPoolProperties.setProperty("jdbc-1.proxool.house-keeping-test-sql", resource.getString("jdbc-1.proxool.house-keeping-test-sql"));
            dbPoolProperties.setProperty("jdbc-1.proxool.maximum-connection-count", resource.getString("jdbc-1.proxool.maximum-connection-count"));
            dbPoolProperties.setProperty("jdbc-1.proxool.minimum-connection-count", resource.getString("jdbc-1.proxool.minimum-connection-count"));
            dbPoolProperties.setProperty("jdbc-1.proxool.maximum-connection-lifetime", resource.getString("jdbc-1.proxool.maximum-connection-lifetime"));
            dbPoolProperties.setProperty("jdbc-1.proxool.simultaneous-build-throttle", resource.getString("jdbc-1.proxool.simultaneous-build-throttle"));
            dbPoolProperties.setProperty("jdbc-1.proxool.recently-started-threshold", resource.getString("jdbc-1.proxool.recently-started-threshold"));
            dbPoolProperties.setProperty("jdbc-1.proxool.overload-without-refusal-lifetime", resource.getString("jdbc-1.proxool.overload-without-refusal-lifetime"));
            dbPoolProperties.setProperty("jdbc-1.proxool.maximum-active-time", resource.getString("jdbc-1.proxool.maximum-active-time"));
            dbPoolProperties.setProperty("jdbc-1.proxool.verbose", resource.getString("jdbc-1.proxool.verbose"));
            dbPoolProperties.setProperty("jdbc-1.proxool.trace", resource.getString("jdbc-1.proxool.trace"));
            dbPoolProperties.setProperty("jdbc-1.proxool.fatal-sql-exception", resource.getString("jdbc-1.proxool.fatal-sql-exception"));
            dbPoolProperties.setProperty("jdbc-1.proxool.prototype-count", resource.getString("jdbc-1.proxool.prototype-count"));
            logger.info("Initization success...");
            isDBPoolInit = true;
            return dbPoolProperties;
        } catch (Exception e) {
            logger.error("Initialization failed", e);  //Category.java
            isDBPoolInit = false;
            throw e;
        }
    }
    }  
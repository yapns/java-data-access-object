/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author naisiong.yap
 */
public class Info {
    private int id;
    private String name;

    
    public void setId (int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
        
    public void setName (String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Info;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import manager.DBConnection;


/**
 *
 * @author naisiong.yap
 */
public class DAO {
    private static Logger logger = Logger.getLogger(DAO.class.getName());
    private static final String SQL_SELECT_MEMBER_INFO = 
            "SELECT id, username FROM cms.users ORDER BY id ASC";
    
    public static void main(String[] args) throws Exception {
           DAO dao = new DAO();
         List<Info> vo = DAO.getList();     
         //System.out.println(vo.size());
         //System.out.println(vo.get(0).getMsisdn());
         //System.out.println(vo.get(1));
                  
         for (Info member : vo) {
         System.out.println("id : "+member.getId()+ 
                ", Name : " + member.getName());
      }
    }
 
      

 public static List<Info> getList() throws Exception {
      Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Info> resultList = new ArrayList();

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement(SQL_SELECT_MEMBER_INFO);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Info member = new Info();
                member.setId(rs.getInt("id"));
                member.setName(rs.getString("username"));
                resultList.add(member);
            }
        } catch (Exception e) {
            //Throwable.java
            throw e;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return resultList;
 }
}
